package exampleproject;

import java.util.HashMap;

public class ImprHashMap extends HashMap<Object,Integer>{
    public void addOne(Object object){
        if(super.keySet().contains(object)){
            int oldNum= super.get(object);
            int newNum=oldNum++;
            super.remove(object);
            super.put(object, newNum);
        }
        else{throw new IllegalStateException("Can't remove that you don't have, like friends on fb! Love, ImprHashMap");}
    }
   
}