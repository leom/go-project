package exampleproject;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;

public class GameController implements EventHandler<ActionEvent>{
    @FXML 
    private AnchorPane gamePane;

    private GridPane gameGrid;


    @FXML
    private void initialize()
    {
        //This is a nice function that happens whenever we switch Root on the scene. 
        startGame();
    }

    @FXML
    private void leaveGame()throws IOException{
        System.out.println("game left!");
        App.setRoot("Intro");
    }

    @FXML
    private void saveGame()throws IOException{

        System.out.println("game saved!");
        App.setRoot("Intro");
    }

    protected void startGame(){
        //gamePane.getChildren().clear();//This is a nice line that I didn't realize that I have. 

        Game game= new Game();
        //I will probably want to make a system such that I can load games, not only make a new one. 
        //I was unsure about the initialize-thing. In initialize(), I have start game, which is this one. 
        //I probably also want to have a startGame(Game toplay) method which lets me load things I've done before.
        //I'm not sure that I'll get the time for this though. 
        GridPane gameGrid= game.getGrid();
        gamePane.getChildren().add(gameGrid);
        //GamePane is the RootPane for the second one. 

    }

    @Override
    public void handle(ActionEvent event) {
        if(gameGrid.getChildren().contains(event.getSource())){
            System.out.println("stuff!");
            //Let's try this. 
            //It doesn't let me do this. 
            //I do not know why yet. 
        }
        throw new UnsupportedOperationException("Unimplemented method 'handle'");
    }
    
}
