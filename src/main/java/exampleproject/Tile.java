package exampleproject;

import javafx.scene.control.Button;

public class Tile extends Button{
    Character state;
    boolean pressed;
    int xPosInGrid;
    int yPosInGrid;
    //I may have to make a double which says where they actually are on the screen. 

    final static Character[] states= new Character[]{'O','B','W'};

    protected Tile(double x, double y){
        super();
        //This should probably just use an invisibleButton(). When I get that to work, my life is good. 
    }
    //Tile should have four states: 
    //I am pretty sure that it also has position, as the button does. 

    //I think this class does need a constructor. The constructor needs to set the color to white. 
    //This may even be so obvious that I need to understand it well. 
    //I've done enough programming and need to think more now!
    protected void setState(Character newState){
        for(Character letter: states){
            if(letter==newState){
                this.state=newState;
            }
        }
        throw new IllegalArgumentException("This letter isn't a state!");
    }

}

    

