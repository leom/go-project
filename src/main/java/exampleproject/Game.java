package exampleproject;

import java.util.ArrayList;
import java.util.HashMap;

import javafx.css.Style;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Game implements EventHandler<ActionEvent>, GameStuff{

    ArrayList<Tile> tiles;
    //I think I have this stored somewhere else so that is pretty cool. 

    protected final static String black="-fx-background-color: #222222";
    protected final static String white= "-fx-background-color: #deedf3";
    protected final static String boardColor="-fx-background-color: #55d93f";

    private HashMap<String,Player> colorAndPlayers= new HashMap<>();

    protected String nextColor;
    //These are a bit random but I think I like them. 

    private GridPane gridPane;
    //I need to have this one here and I am not exactly sure why I do.
    
    HashMap<Tuple,Button2> coordsAndButtons= new HashMap<>();
    HashMap<Button2,Tuple> buttonsAndCoords= new HashMap<>();

    public GridPane getGrid(){
        return this.gridPane;
    }

    private boolean legalToPaint(Button button){
        return button.getStyle().equals(boardColor);
    }
    
    public void paint(Button2 toPaint){
        if(!legalToPaint(toPaint)) return ;//I want there to be a text for this somewhere. 
        
        toPaint.setStyle(nextColor);
        if(nextColor.equals(black)){
            nextColor=white;
        }
        else if(nextColor.equals(white)){//Lol, putting if instead of else if was some dumb stuff...
            nextColor=black;
        }//There is probably a nicer way of writing this...
    }

    protected int pointsOfPlayer(String player){
        if(!colorAndPlayers.keySet().contains(player)){
            throw new IllegalArgumentException("Write White or Black, those are the players!");
        }
        return colorAndPlayers.get(player).points;
        //I am assuming that I will put this one somewhere and use it. 
    }

    public Game(){

        colorAndPlayers.put("Black",new Player());
        colorAndPlayers.put("White",new Player());
        //I probably have too many HashMaps...


        GridPane newGridPane = new GridPane();
        newGridPane.setMaxWidth(380);
        newGridPane.setMaxHeight(380);
        newGridPane.setLayoutX(0);
        newGridPane.setLayoutY(50);
        //This is where and how big the gridPane is going to be. It si going to be on the gameGrid.
        //This can be seen in the gameController file. 
        nextColor=black;
        //This one goes for all of them...    
        for(int a=0;a<19;a++){
            for(int b=0;b<19;b++){
                Button2 button= new Button2(20);
                
                coordsAndButtons.put(new Tuple(a,b), button);
                buttonsAndCoords.put(button, new Tuple(a, b));
                button.setStyle(boardColor);
                //This didn't work and I understand why now. It was super silly and held no merit. 
                button.setOnAction(this);

                newGridPane.add(button,a,b,1,1);
            }
        }
        
        this.gridPane= newGridPane;
    }//I think this function is probably pretty good. I am not sure. 


    private Button2 getButton(int x, int y){
        for(Tuple tuple:coordsAndButtons.keySet()){
            if(tuple.xCoord==x&&tuple.yCoord==y){
                return coordsAndButtons.get(tuple);
            }
        }
        return null; 
    }
    //I am not sure that these two are meant to be private. I haven't thought about it well yet. 
    //I am getting quite a bit better nowadays and I like that. 
    private Tuple getTuple(int x, int y){
        for(Tuple tuple:coordsAndButtons.keySet()){
            if(tuple.xCoord==x&&tuple.yCoord==y){
                return tuple;
            }
        }
        return null; 

    }

    public boolean acceptableRowNum(int a){
        return a>=0||a<=18;
    }
    public ArrayList<Button2> findPileOfButton2s(Button2 toLookFrom){
        ArrayList<Button2> Pile= new ArrayList<>();
        boolean finished=false;
        while(finished==false){
            ArrayList<Button2> newAdjcs=findAdjacencies(Pile);
            if(newAdjcs.size()>0)Pile.addAll(newAdjcs);
            else{finished=true;}
        }
        return Pile;
    }//Hopefully, this piece of code should find the Pile that I am looking for. 
    //It is probably a pretty bad piece of code to be fair. 
    //I could think a lot about optimizing it. 

    private ArrayList<Button2> findAdjacencies(ArrayList<Button2> toComeFrom){
        ArrayList<Button2> adjacents= new ArrayList<>();
        String color=toComeFrom.get(0).getStyle();
        for(Button2 button:toComeFrom){
            for(Button2 buttonToCheck:findAdjacents(button)){
                if(buttonToCheck.getStyle()==color||!adjacents.contains(buttonToCheck)){
                    adjacents.add(buttonToCheck);
                    
                }
            }
        }
        return adjacents;
        //I have it in three levels. That is not very good. 
    }
    private ArrayList<Button2> findAdjacents(int x, int y){
        if(!numbersFine(x,y)){throw new IllegalArgumentException("Tall utenfor");}

        ArrayList<Button2> returnList= new ArrayList<>();
        
        if(numbersFine(x+1,y)) returnList.add(getButton(x+1,y)); 
        if(numbersFine(x-1,y)) returnList.add(getButton(x-1,y)); 
        if(numbersFine(x,y+1)) returnList.add(getButton(x,y+1)); 
        if(numbersFine(x,y-1)) returnList.add(getButton(x,y-1)); 

        return returnList;

    }

    private ArrayList<Button2> findAdjacents(Button2 button){
        Tuple tuple= buttonsAndCoords.get(button);
        int x= tuple.xCoord;
        int y= tuple.yCoord;
        return findAdjacents(x, y);
        //Bruv this was a nice litte trick. 

    }


    private boolean numbersFine(Tuple ab){
        return acceptableRowNum(ab.xCoord)&&acceptableRowNum(ab.yCoord);
    }
    //I do this one with both Tuple and numbers. I'm not sure that Tuples are that good. 
    //The indexing I did in the list may just be a better system. 
    private boolean numbersFine(int a, int b){
        return acceptableRowNum(a)&&acceptableRowNum(b);
    }

    private class Tuple{
        int xCoord;
        int yCoord;
        private Tuple(int x, int y){
            this.xCoord=x;
            this.yCoord=y;
        }

    }

    @Override
    public void handle(ActionEvent event) {
        if(event.getSource() instanceof Button2){
            Button2 button= (Button2)event.getSource();
            paint(button);
        }
                       
    }
        
     
    
}
