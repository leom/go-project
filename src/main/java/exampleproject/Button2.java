package exampleproject;

import javafx.scene.control.Button;

public class Button2 extends Button{
    public void setHeight(int a){
        super.setMaxHeight(a);
        super.setMinHeight(a);
    }
    public void setWidth(int a){
        super.setMaxWidth(a);
        super.setMinWidth(a);
    }
    public Button2(int heightAndWeight){
        super();
        setHeight(heightAndWeight);
        setWidth(heightAndWeight);
    }

}
