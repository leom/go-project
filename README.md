Her ligger implementasjonen min av spillet Go. 
Go er et asiatisk spill som handler om at man får poeng og fjerner brikker ved å omringe et helt sett med brikker. 
Spillet avsluttes når begge spillerene ikke ønsker å legge flere brikker fordi det vil svekke posisjonen deres.
Poengsum regnes ut som en kombinasjon av område som hver av spillerene har omringet og hvor mange av brikkene hver av dem har tatt. 
Min variant av spillet har ikke implementert utregningen av areal eller avslutningsfunksjonen. Den har implementert at man kan omringe brikkene til den andre spilleren slik at de blir borte og du får poeng. 
Den skal ha implementert at man får poeng av dette. 
Disse poengene blir lagret, selv om det ikke er gjort helt ennå.
